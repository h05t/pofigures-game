﻿using UnityEngine;
using System.Collections;

// from http://zhorick.ru/?p=14

public class CameraController : MonoBehaviour {

	public float camSpeed; // скорость перемещения камеры в плоскости
	public float zoomSpeed; // скорость зума колесом мыши
	public float mouseSensitivity; // чувствительность мыши при вращении камеры 
	
	private Transform _myTransform; //здесь кэширую свойство transform камеры

	// Ограничители движения камеры
	public float moveDistanceMin, moveDistanceMax;
	public float zoomDistanceMin, zoomDistanceMax;
	//private float[] distances = new float[32];
	//private Vector3 moveDirection = Vector3.zero;
	private Vector3 zoomDirection = Vector3.zero;

	// Use this for initialization
	void Start () {

		_myTransform = transform;

	}

	// Update is called once per frame
	void Update () {
	
		//преобразуем скорость по фреймам в скорость по времени
		float speedMod = camSpeed * Time.deltaTime;
		float zoomMod = zoomSpeed * Time.deltaTime;
		float sensMod = mouseSensitivity * Time.deltaTime;
		
		//движение камеры влево-вправо
		/*moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, 0);
		moveDirection *= speedMod;
		//Checking the upper and lower bounds is a matter of determining if
		// the direction we're moving is positive (zooming in) or negative (zooming out).
		if (Input.GetAxis("Horizontal") > 0)
		{
			//We shouldn't be allowed to zoom in more than distanceMin
			if (Mathf.Floor(transform.position.x + moveDirection.x) > moveDistanceMin)
			{
				transform.Translate(moveDirection, Space.Self);
			}
		} 
		else if (Input.GetAxis("Horizontal") < 0) 
		{
			//We shouldn't be allowed to zoom out more than distanceMax
			if (Mathf.Floor(transform.position.x + moveDirection.x) < moveDistanceMax)
			{
				transform.Translate(moveDirection, Space.Self);
			}
		}*/

		_myTransform.Translate(Vector3.right * Input.GetAxis("Horizontal") * speedMod, Space.Self); 
		
		//уже поинтересней, движение вперед-назад
		_myTransform.Translate(Vector3.Scale(_myTransform.TransformDirection(Vector3.forward), new Vector3(1,0,1)).normalized *
		                       Input.GetAxis("Vertical") * speedMod, Space.World); 

		// Ограничение зума камеры верхней и нижней границами 
		// from -- http://answers.unity3d.com/questions/322455/zooming-to-mouse-position-with-scroll-wheel.html
		zoomDirection = new Vector3(0,0,Input.GetAxis("Mouse ScrollWheel"));
		zoomDirection *= zoomMod;
		//Checking the upper and lower bounds is a matter of determining if
		// the direction we're moving is positive (zooming in) or negative (zooming out).
		if (Input.GetAxis("Mouse ScrollWheel") > 0)
		{
			//We shouldn't be allowed to zoom in more than distanceMin
			if (Mathf.Floor(transform.position.y + zoomDirection.y) > zoomDistanceMin)
			{
				transform.Translate(zoomDirection, Space.Self);
			}
		} 
		else if (Input.GetAxis("Mouse ScrollWheel") < 0) 
		{
			//We shouldn't be allowed to zoom out more than distanceMax
			if (Mathf.Floor(transform.position.y + zoomDirection.y) < zoomDistanceMax)
			{
				transform.Translate(zoomDirection, Space.Self);
			}
		}


		//поворот камеры
		if (Input.GetMouseButton(1))
		{
			_myTransform.Rotate(-Input.GetAxis("Mouse Y") * sensMod, 0, 0, Space.Self);
			_myTransform.Rotate(0, Input.GetAxis("Mouse X") * sensMod, 0, Space.World);
		}



	}
}
