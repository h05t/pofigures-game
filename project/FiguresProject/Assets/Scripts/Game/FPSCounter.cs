﻿//
// Denis Solomko - silent bob
// 

using UnityEngine;
using System.Collections;

public class FPSCounter : MonoBehaviour
{
	int frameCounter = 0;
	float timeElapsed = 0f;
	float fps = 0f;
	float updateInterval = 0.5f;
	
	//=================================================================================================
	
	void Start()
	{
		
	}
	
	void Update()
	{
		if (Time.timeScale < 0.0001f)
		{
			// may be make something like GetTimeMillis() later to count deltaTime
			//guiText.text = "pause"; // System.String.Format("{0:F1}", fps);
		}
		else
		{
			frameCounter++;
			timeElapsed += Time.deltaTime;
			
			// refresh counter every updateInterval seconds
			if (timeElapsed >= updateInterval)
			{
				
				fps = frameCounter / timeElapsed;
				
				timeElapsed = 0.0f;
				frameCounter = 0;
				
				//guiText.text = System.String.Format("{0:F1}", fps);
			}
		}
	}
	
	public float getFPS()
	{
		return fps;
	}
	
} // class FPSCounter