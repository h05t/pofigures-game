﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

// useful links
//  
// web- http://answers.unity3d.com/questions/208510/systemiofile-error-take-a-screenshot.html


public class SaveScreen : MonoBehaviour {

	public Camera mainCam;
	public bool showText = true;
	string path;
	public string directoryName = "Screens";
	string mainPath = "";
	string cName;
	int i = 0;

	void Start()
	{
		path = Application.dataPath + "/";
		mainPath = path + directoryName + "/";

		if (!Directory.Exists(mainPath)) {
			Directory.CreateDirectory(mainPath);
		}
	}

	IEnumerator SaveScreenShot(string name) {

		int screenWidth = (int)Screen.width;
		int screenHeight = (int)Screen.height;

		RenderTexture rt = new RenderTexture (screenWidth, screenHeight, 0);
		mainCam.targetTexture = rt;
		Texture2D sc = new Texture2D (screenWidth, screenHeight, TextureFormat.RGB24, false);
		mainCam.Render ();

		yield return new WaitForSeconds (0.3f);
		showText = true;

		RenderTexture.active = rt;
		sc.ReadPixels (new Rect(0, 0, screenWidth, screenHeight), 0, 0);
		mainCam.targetTexture = null;
		RenderTexture.active = null;
		Destroy (rt);

		//byte[] bytes = sc.EncodeToPNG ();
		//string finalPath = mainPath + name + ".png";
		//File.WriteAllBytes(finalPath, bytes);	// в вебе не работает !!!

		yield return new WaitForSeconds (2.3f);
		showText = false;

	}

	void OnGUI() 
	{
		if (showText) 
		{
			GUI.Label(new Rect(100, 100, 200, 50), "Saving screen!");
		}
	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Q)) {
			i++;
			cName = "Screen_" + i;
			StartCoroutine(SaveScreenShot(cName));
		}
	}

}
