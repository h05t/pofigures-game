﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using System.Linq;

public class OrderScript : MonoBehaviour {

	public string orderFilename; // path

	public Texture2D backgroundTexture;  	// фон для режима меню

	private string name_of_building = "";	// будет создаваться папка с именем и туда все сохранятся
	private string username = "";
	private string telephone = "";
	private string email = ""; // можно отсылать на мыло письмо с подтверждением!
	private GUISkin skin;
	private bool menuMode = true;	// включено ли меню
	private bool orderCreated = false;

	// Use this for initialization
	void Start () 
	{
		skin = Resources.Load ("OrderSceneGUISkin") as GUISkin;

		if (orderFilename == "") orderFilename = "ClientsOrders.txt";
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI()
	{
		GUI.skin = skin;

		if (menuMode) 
		{
			if (backgroundTexture != null)
				GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), backgroundTexture);

			GUILayout.BeginHorizontal();
			GUILayout.Label ("Больших кубиков " + CubeCounterSingleton.GetInstance().BigCubeNum.ToString() + ":");
			if(0 != CubeCounterSingleton.GetInstance().bigBlueCubeNum)
				GUILayout.Label ("синих: " + CubeCounterSingleton.GetInstance().bigBlueCubeNum.ToString());
			if(0 != CubeCounterSingleton.GetInstance().bigGreenCubeNum)
				GUILayout.Label ("зеленых: " + CubeCounterSingleton.GetInstance().bigGreenCubeNum.ToString());
			if(0 != CubeCounterSingleton.GetInstance().bigYellowCubeNum)
				GUILayout.Label ("желтых: " + CubeCounterSingleton.GetInstance().bigYellowCubeNum.ToString());
			if(0 != CubeCounterSingleton.GetInstance().bigWhiteCubeNum)
				GUILayout.Label ("белых: " + CubeCounterSingleton.GetInstance().bigWhiteCubeNum.ToString());
			if(0 != CubeCounterSingleton.GetInstance().bigRedCubeNum)
				GUILayout.Label ("красных: " + CubeCounterSingleton.GetInstance().bigRedCubeNum.ToString());
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			GUILayout.Label ("Маленьких кубиков " + CubeCounterSingleton.GetInstance().SmCubeNum.ToString() + ":");
			if(0 != CubeCounterSingleton.GetInstance().smBlueCubeNum)
                GUILayout.Label("синих: " + CubeCounterSingleton.GetInstance().smBlueCubeNum.ToString());
			if(0 != CubeCounterSingleton.GetInstance().smGreenCubeNum)
				GUILayout.Label ("зеленых: " + CubeCounterSingleton.GetInstance().smGreenCubeNum.ToString());
			if(0 != CubeCounterSingleton.GetInstance().smYellowCubeNum)
				GUILayout.Label ("желтых: " + CubeCounterSingleton.GetInstance().smYellowCubeNum.ToString());
			if(0 != CubeCounterSingleton.GetInstance().smWhiteCubeNum)
				GUILayout.Label ("белых: " + CubeCounterSingleton.GetInstance().smWhiteCubeNum.ToString());
			if(0 != CubeCounterSingleton.GetInstance().smRedCubeNum)
				GUILayout.Label ("красных: " + CubeCounterSingleton.GetInstance().smRedCubeNum.ToString());
			GUILayout.EndHorizontal();

			GUILayout.Label ("Всего кубиков: " + CubeCounterSingleton.GetInstance().totalNum.ToString());
			GUILayout.Label ("Вес конструкции: " + CubeCounterSingleton.GetInstance().GetTotalWeight().ToString() + " (кг)");

			// order menu
			GUI.BeginGroup(new Rect (Screen.width / 2 - 200 / 2, Screen.height / 2 - 180, 200, 500));
				GUILayout.Label ("Имя строения:");
				name_of_building = GUILayout.TextField (name_of_building, 100);
				GUILayout.Label ("Имя клиента:");
				username = GUILayout.TextField (username, 100);
				GUILayout.Label ("Телефон:");
				telephone = GUILayout.TextField (telephone, 100);
				GUILayout.Label ("Электронная почта:");
				email = GUILayout.TextField (email, 100);
				//confirm = GUI.PasswordField (new Rect (300, 189, 200, 25), confirm, "*"[0], 40);
				//

				// 
				// 
				// Кнопка заказа конструкции
				//  - контактная информация для сбора конструкции
				//  - 
				//
				GUI.contentColor = Color.green;
				
				if (GUILayout.Button ("Заказать")) 
				{
					string orderStr = "Название конструкции - " + name_of_building + ";\n";
                    orderStr += "Имя клиента - " + username + ";\n";
					orderStr += "Телефон - " + telephone + ";\n";
					orderStr += "Электронная почта - " + email + ";\n";

                    if (0 != CubeCounterSingleton.GetInstance().BigCubeNum)
                        orderStr += "Всего больших кубиков: " + CubeCounterSingleton.GetInstance().BigCubeNum.ToString() + ";\n";
                    if (0 != CubeCounterSingleton.GetInstance().bigBlueCubeNum)
                        orderStr += "   синих: " + CubeCounterSingleton.GetInstance().bigBlueCubeNum.ToString() + ";\n";
                    if (0 != CubeCounterSingleton.GetInstance().bigGreenCubeNum)
                        orderStr += "   зеленых: " + CubeCounterSingleton.GetInstance().bigGreenCubeNum.ToString() + ";\n";
                    if (0 != CubeCounterSingleton.GetInstance().bigYellowCubeNum)
                        orderStr += "   желтых: " + CubeCounterSingleton.GetInstance().bigYellowCubeNum.ToString() + ";\n";
                    if (0 != CubeCounterSingleton.GetInstance().bigWhiteCubeNum)
                        orderStr += "   белых: " + CubeCounterSingleton.GetInstance().bigWhiteCubeNum.ToString() + ";\n";
                    if (0 != CubeCounterSingleton.GetInstance().bigRedCubeNum)
                        orderStr += "   красных: " + CubeCounterSingleton.GetInstance().bigRedCubeNum.ToString() + ";\n";

                    if (0 != CubeCounterSingleton.GetInstance().SmCubeNum)
                        orderStr += "Всего маленьких кубиков: " + CubeCounterSingleton.GetInstance().SmCubeNum.ToString() + ";\n";
                    if (0 != CubeCounterSingleton.GetInstance().smBlueCubeNum)
                        orderStr += "   синих: " + CubeCounterSingleton.GetInstance().smBlueCubeNum.ToString() + ";\n";
                    if (0 != CubeCounterSingleton.GetInstance().smGreenCubeNum)
                        orderStr += "   зеленых: " + CubeCounterSingleton.GetInstance().smGreenCubeNum.ToString() + ";\n";
                    if (0 != CubeCounterSingleton.GetInstance().smYellowCubeNum)
                        orderStr += "   желтых: " + CubeCounterSingleton.GetInstance().smYellowCubeNum.ToString() + ";\n";
                    if (0 != CubeCounterSingleton.GetInstance().smWhiteCubeNum)
                        orderStr += "   белых: " + CubeCounterSingleton.GetInstance().smWhiteCubeNum.ToString() + ";\n";
                    if (0 != CubeCounterSingleton.GetInstance().smRedCubeNum)
                        orderStr += "   красных: " + CubeCounterSingleton.GetInstance().smRedCubeNum.ToString() + ";\n";

                    orderStr += "Всего кубиков: " + CubeCounterSingleton.GetInstance().totalNum.ToString() + ";\n";
                    orderStr += "Вес конструкции: " + CubeCounterSingleton.GetInstance().GetTotalWeight().ToString() + " (кг);\n";

					orderStr += "----------------------\n";
					// warning: security hole !!!
					StartCoroutine(UploadTXT("http://funnybucksbunny.hol.es/FileOrder.php", orderStr, name_of_building));	// ф-я замены пробелов на / для переменных

					Debug.Log (name_of_building + ", " + username + ", " + email);
				}

				GUI.contentColor = Color.red;
	
				// TODO TODO TODO
				// - back function
				// 
				// Back to previous scene
				if (GUILayout.Button ("Назад")) 
				{
				// go back -- http://answers.unity3d.com/questions/202432/load-last-scenelevel.html
					Application.LoadLevel("MainScene");
				}
				//
				// Label for order creation 
				if (orderCreated) {
					GUI.Label(new Rect(10, 0, 300, 20), "Order is created!");
				}
		
			GUI.EndGroup ();

		}
	}

	/*
	 * 1. кнопка для начала заного игры
	 * 2. кнопка возвращения на старую сцену
	 * 3. 
	 * 
	 */

	// Upload texture to server by URL
	public IEnumerator UploadTXT(string url, string content, string buildingDir)
	{
		float startTime = Time.time;

		// show lable 2 seconds
		yield return new WaitForSeconds (0.3f);
		orderCreated = true;

		yield return new WaitForEndOfFrame(); // чейто??????????????????????????????????????????

		// Переводим стрингу в байт:
		byte[] txtData = new byte[content.Length * sizeof(char)];
		System.Buffer.BlockCopy(content.ToCharArray(), 0, txtData, 0, txtData.Length);

		// send to server by WWWForm
		if (txtData != null) 
		{
			WWWForm form = new WWWForm();

			// директория для заказа
			form.AddField("dirName", buildingDir);
			// данные которые передаем в форме
			form.AddBinaryData("orderTxtFile", txtData, "CustomerOrders.txt", "txt");
			
			WWW www = new WWW(url, form);
			StartCoroutine(WaitForRequest(www));
		}
		
		yield return new WaitForSeconds (2.3f);
		orderCreated = false;
		//screenshotCam.enabled = false;
		
		float endTime = Time.time - startTime;
		Debug.Log ("Taking order time: " + endTime.ToString("0.000") + " seconds");
	}

	// WWW request answer
	IEnumerator WaitForRequest(WWW www) 
	{
		yield return www;
		if (www.error != null) 
			Debug.Log("Order error: " + www.error);
		else 
			Debug.Log(www.text);
	}
}	// class
