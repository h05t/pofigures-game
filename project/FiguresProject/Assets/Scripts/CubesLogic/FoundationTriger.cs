﻿using UnityEngine;
using System.Collections;

public class FoundationTriger : MonoBehaviour
{
   // Счетчик коллайдеров с тегом "BlockCeiling",
   // на которых сейчас стоит фантом.
   public int j = 0;

   // Сбрасывает состояние на сост. по умолчанию,
   // чтобы после вытаскивания из пулла объект выглядел, 
   // как будто его создали с помощ. new:
   void OnEnable()
   {
      j = 0;
   }

   void OnTriggerEnter(Collider other)
   {
      if ("BlockCeiling" == other.tag)
      {
         ++j;
         // Здесь проверка, озер ниже фантома (т.е. фантом соит 
         // на озере, а не пересекает его на одном уровне)
         this.gameObject.layer = 9;
      }
   }

   void OnTriggerExit(Collider other)
   {
      if ("BlockCeiling" == other.tag)
      {
         DecrimentBlockCounter();
      }
   }

   public void DecrimentBlockCounter()
   {
      if (0 < j)
      {
         --j;
         if(0 == j)
            this.gameObject.layer = 8;
      }
   }
}
