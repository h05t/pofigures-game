﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;

public class CubeCounterSingleton
{
	private Dictionary<string, Material> MaterialDictionary = new Dictionary<string, Material>();
	private static CubeCounterSingleton cubeCounter = new CubeCounterSingleton();
	private CubeCounterSingleton()
	{
		EraseCounter();
	}

	public static CubeCounterSingleton GetInstance()
	{
		return cubeCounter;
	}

	// Заполняем словарь нашими(отличными от стандартных) оттенками
	// цветов, и ставим им в соответствие названия текстур:
	public void SetMaterialDictionary(Material[] cube_materials)
	{ 
		for(int i = 0; i < cube_materials.Length; i++)
			MaterialDictionary.Add(cube_materials[i].name, cube_materials[i]);
	}

	public void incrementCube(GameObject cube)
	{
		// Для определения размера кубика:
		Vector3 blockScale = cube.transform.localScale;
		Color cubeColor = cube.renderer.material.color;

		if (4f == blockScale.x) 
		{
			BigCubeNum++;

			cubeColor.a = MaterialDictionary["BlueCubeMaterial"].color.a;
			if(MaterialDictionary["BlueCubeMaterial"].color == cubeColor)
				bigBlueCubeNum++;

			cubeColor.a = MaterialDictionary["GreenCubeMaterial"].color.a;
			if(MaterialDictionary["GreenCubeMaterial"].color == cubeColor)
				bigGreenCubeNum++;

			cubeColor.a = MaterialDictionary["RedCubeMaterial"].color.a;
			if(MaterialDictionary["RedCubeMaterial"].color == cubeColor)
				bigRedCubeNum++;

			cubeColor.a = MaterialDictionary["WhiteCubeMaterial"].color.a;
			if(MaterialDictionary["WhiteCubeMaterial"].color == cubeColor)
				bigWhiteCubeNum++;

			cubeColor.a = MaterialDictionary["YellowCubeMaterial"].color.a;
			if(MaterialDictionary["YellowCubeMaterial"].color == cubeColor)
				bigYellowCubeNum++;
		}
		else
			if (2f == blockScale.x) 
			{
				SmCubeNum++;

				cubeColor.a = MaterialDictionary["BlueCubeMaterial"].color.a;
				if(MaterialDictionary["BlueCubeMaterial"].color == cubeColor)
					smBlueCubeNum++;
				
				cubeColor.a = MaterialDictionary["GreenCubeMaterial"].color.a;
				if(MaterialDictionary["GreenCubeMaterial"].color == cubeColor)
					smGreenCubeNum++;
				
				cubeColor.a = MaterialDictionary["RedCubeMaterial"].color.a;
				if(MaterialDictionary["RedCubeMaterial"].color == cubeColor)
					smRedCubeNum++;
				
				cubeColor.a = MaterialDictionary["WhiteCubeMaterial"].color.a;
				if(MaterialDictionary["WhiteCubeMaterial"].color == cubeColor)
					smWhiteCubeNum++;
				
				cubeColor.a = MaterialDictionary["YellowCubeMaterial"].color.a;
				if(MaterialDictionary["YellowCubeMaterial"].color == cubeColor)
					smYellowCubeNum++;
			}
		totalNum = SmCubeNum + BigCubeNum;
	}

	public void decrementCube(GameObject cube)
	{
		// Для определения размера кубика:
		Vector3 blockScale = cube.transform.localScale;
		Color cubeColor = cube.renderer.material.color;
		
		if (4f == blockScale.x) 
		{
			BigCubeNum--;

			cubeColor.a = MaterialDictionary["BlueCubeMaterial"].color.a;
			if(MaterialDictionary["BlueCubeMaterial"].color == cubeColor)
				bigBlueCubeNum--;
			
			cubeColor.a = MaterialDictionary["GreenCubeMaterial"].color.a;
			if(MaterialDictionary["GreenCubeMaterial"].color == cubeColor)
				bigGreenCubeNum--;
			
			cubeColor.a = MaterialDictionary["RedCubeMaterial"].color.a;
			if(MaterialDictionary["RedCubeMaterial"].color == cubeColor)
				bigRedCubeNum--;
			
			cubeColor.a = MaterialDictionary["WhiteCubeMaterial"].color.a;
			if(MaterialDictionary["WhiteCubeMaterial"].color == cubeColor)
				bigWhiteCubeNum--;
			
			cubeColor.a = MaterialDictionary["YellowCubeMaterial"].color.a;
			if(MaterialDictionary["YellowCubeMaterial"].color == cubeColor)
				bigYellowCubeNum--;
		}
		else
			if (2f == blockScale.x) 
		{
			SmCubeNum--;

			cubeColor.a = MaterialDictionary["BlueCubeMaterial"].color.a;
			if(MaterialDictionary["BlueCubeMaterial"].color == cubeColor)
				smBlueCubeNum--;
			
			cubeColor.a = MaterialDictionary["GreenCubeMaterial"].color.a;
			if(MaterialDictionary["GreenCubeMaterial"].color == cubeColor)
				smGreenCubeNum--;
			
			cubeColor.a = MaterialDictionary["RedCubeMaterial"].color.a;
			if(MaterialDictionary["RedCubeMaterial"].color == cubeColor)
				smRedCubeNum--;
			
			cubeColor.a = MaterialDictionary["WhiteCubeMaterial"].color.a;
			if(MaterialDictionary["WhiteCubeMaterial"].color == cubeColor)
				smWhiteCubeNum--;
			
			cubeColor.a = MaterialDictionary["YellowCubeMaterial"].color.a;
			if(MaterialDictionary["YellowCubeMaterial"].color == cubeColor)
				smYellowCubeNum--;
		}
		totalNum = SmCubeNum + BigCubeNum;
	}

	// Возвращает вес всей конструкции в килограммах:
	public float GetTotalWeight()
	{
		float totalWeight;
		totalWeight = BigCubeNum * BigCubeWeight;
		totalWeight += SmCubeNum * SmCubeWeight;

		// Перводим в колограммы:
		totalWeight = totalWeight / 1000;

		return totalWeight;
	}

	public void EraseCounter()
	{
		smWhiteCubeNum = 0;
		smBlueCubeNum = 0;
		smGreenCubeNum = 0;
		smRedCubeNum = 0;
		smYellowCubeNum = 0;
		
		bigWhiteCubeNum = 0;
		bigBlueCubeNum = 0;
		bigGreenCubeNum = 0;
		bigRedCubeNum = 0;
		bigYellowCubeNum = 0;
		
		BigCubeNum = 0;
		SmCubeNum = 0;
		totalNum = 0;
	}

	public int smWhiteCubeNum;
	public int smBlueCubeNum;
	public int smGreenCubeNum;
	public int smRedCubeNum;
	public int smYellowCubeNum;

	public int bigWhiteCubeNum;
	public int bigBlueCubeNum;
	public int bigGreenCubeNum;
	public int bigRedCubeNum;
	public int bigYellowCubeNum;

	public int BigCubeNum;
	public int SmCubeNum;
	public int totalNum;

	private const int SmCubeWeight = 33; //(весс в граммах)
	private const int BigCubeWeight = 63; //(весс в граммах)
}

public class BlockManager : MonoBehaviour 
{
	public Material lightCubesAnimatedMat;
    public Material darkCubesAnimatedMat;
	public GameObject fantomInstanse;
	private float maxDistanceCreateBlock = 200f;
	private const uint xyzMat = 64; 

	// Для включения/выключения выделения кубика рамкой:
	private GameObject previosCube;

	// Для включения/выключения выделения кубика рамкой:
	private Vector3 lastBuildedCubePos;

	void Start()
	{
	}
	
	public bool CreateBlock(GameObject block)
	{
		/// Попытка игнорировать фантомный кубик лучем:
		// Bit shift the index of the layer (8) to get a bit mask
		int layerMask = 1 << 8;
		
		// This would cast rays only against colliders in layer 8.
		// But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
		layerMask = ~layerMask;

		// Кубик можно строить, только если нет пересечения с фантомом:
      PhantomConditions fantomTrigger = fantomInstanse.GetComponent<PhantomConditions>();

		// NOTE: лучи лучше создавать в Start()
		RaycastHit hit; 
		if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, maxDistanceCreateBlock, layerMask))
            if (hit.collider != null && (hit.collider.tag == "Block" || hit.collider.tag == "Plane" || hit.collider.tag == "PhFoundation") && lastBuildedCubePos != fantomInstanse.transform.position && fantomTrigger.permittedToConstruct )
			{ 
				Vector3 pos = fantomInstanse.transform.position;

				GameObject tmpPooledObj = PoolManager.Spawn(block.name);  

				//GameObject Combiner = GameObject.Find("Combiner");
				//tmpPooledObj.transform.parent = Combiner.transform;

				// Смещаем объект вытащенный из пулла в позицию, где сейчас стоит фантом, 
				// и разворачиваем соответственно углу фантома 
				tmpPooledObj.transform.position = pos;
				tmpPooledObj.transform.rotation = fantomInstanse.transform.rotation;

                //// Передаем материал от фантома:
                Shader tmpShared = tmpPooledObj.renderer.material.shader;
                tmpPooledObj.renderer.material = fantomInstanse.renderer.material;
                tmpPooledObj.transform.GetChild(0).renderer.material = fantomInstanse.renderer.material;

                tmpPooledObj.renderer.material.shader = tmpShared;
                tmpPooledObj.transform.GetChild(0).renderer.material.shader = tmpShared;

				// Выставляем цвет объекта в соответсвтвии текущим с цветом фантома.
				Color tmpColor = fantomInstanse.renderer.material.color;
				tmpColor.a = tmpPooledObj.renderer.material.color.a;
                tmpPooledObj.renderer.material.color = tmpColor;
                tmpPooledObj.transform.GetChild(0).renderer.material.color = tmpColor;

				// Если куб светлых тонов, то и его анимированный таргет меняем на светлый:
				//if(проверять имя материала. Если белый или желтый то меняем таргет)
                if ("RedCubeMaterial (Instance)" == fantomInstanse.renderer.material.name || // строковые константы вынести в глобальные!!!!!!!!!!!!!!!!!!!!!!
                   "GreenCubeMaterial (Instance)" == fantomInstanse.renderer.material.name ||
                   "BlueCubeMaterial (Instance)" == fantomInstanse.renderer.material.name)
                    tmpPooledObj.transform.GetChild(1).renderer.material = lightCubesAnimatedMat;
                else
                    if ("WhiteCubeMaterial (Instance)" == fantomInstanse.renderer.material.name ||
                       "YellowCubeMaterial (Instance)" == fantomInstanse.renderer.material.name)
                        tmpPooledObj.transform.GetChild(1).renderer.material = darkCubesAnimatedMat;
                
				// Добавляем тип кубика и его цвет в счетчик:
				CubeCounterSingleton.GetInstance().incrementCube(tmpPooledObj);
                //tmpPooledObj.rigidbody.Sleep();

				// Запоминаем координаты последнего построенного кубика.
				// Если эти координаты совпадают с текущими координатами
				// фантома, то строить в этом месте нельзя.
				lastBuildedCubePos = tmpPooledObj.transform.position;

				
				//Combiner.SendMessage("Combine");

                //// Запоминаем начальное положение объекта:
                //MakeSleep tmpPooledObj2 = tmpPooledObj.GetComponent<MakeSleep>();
                //tmpPooledObj2.
			}
			return false;
	}

	public string DestroyBlock()
	{
		int layerMask = 11 << 8;
		layerMask = ~layerMask;

        // Кубик можно удалять, только если нет пересечения с фантомом:
      /*PhantomConditions fantomTrigger = fantomInstanse.GetComponent<PhantomConditions>();*/ // Временное решение!!!!! Пока не найдется способ уведомлять об удалении фантома объектом, который (объ) нах внутри фант!!!

		RaycastHit hit;
		if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, maxDistanceCreateBlock, layerMask))
            if (hit.collider != null && hit.collider.tag == "Block" && lastBuildedCubePos != fantomInstanse.transform.position /*&& fantomTrigger.permittedToConstruct*/)
			{			
				// Удаляем тип кубика и его цвет из счетчика:
				CubeCounterSingleton.GetInstance().decrementCube(hit.collider.gameObject);
                //Debug.Log("name = " + hit.collider.gameObject.name);

				//if(lastBuildedCubePos == hit.collider.gameObject.transform.position)
					lastBuildedCubePos.Set(0,0,0);

				// Сообщаем объекту, что сейчас его удалят:
				//MakeSleep po = hit.collider.gameObject.GetComponent<MakeSleep>();
				//po.PrepareToDespawn();

                // Перед удалением, кубик должен разбудить все кубики, который стоят на нем:
                StartCoroutine(DespawnCube(hit.collider.gameObject));

				//return true;
			}
        return hit.collider.gameObject.name;
	}

    private IEnumerator DespawnCube(GameObject go) 
	{
		go.rigidbody.WakeUp();
		yield return new WaitForFixedUpdate();
        yield return new WaitForFixedUpdate();

		PoolManager.Despawn(go);
	}

	public void WithFantom()
	{
		int layerMask = 1 << 8;
		layerMask = ~layerMask;

		RaycastHit hit;
		Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, maxDistanceCreateBlock, layerMask);

      if (hit.collider != null && (hit.collider.tag == "Block" || hit.collider.tag == "Plane" || hit.collider.tag == "PhFoundation"))  
		{
			// Запоминаем точку пересечения плоскости
			// с самим лучем:
			Vector3 pos = hit.point;
			  
			pos = hit.point;
			pos.y = Mathf.Round(pos.y) + 1f;
			pos.x = Mathf.Round(pos.x);
			pos.z = Mathf.Round(pos.z);

			// Передвигать нужно тогда когда это новые координаты!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                fantomInstanse.transform.GetChild(4).rigidbody.MovePosition(pos);
		}

        // Запускаем луч, который игнорирует весь
        // фантом, вместе с его основанием:
		layerMask = 11 << 8;
		layerMask = ~layerMask;

		Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, maxDistanceCreateBlock, layerMask);

        if (hit.collider != null && (hit.collider.tag == "Block" || hit.collider.tag == "Plane"))
        {
            // Выделиние кубика рамкой: // Перенести в объект кубика!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if ((hit.collider.tag == "Block") && (false == hit.collider.transform.GetChild(1).gameObject.activeSelf))
            {
                if (null != previosCube)
                    previosCube.transform.GetChild(1).gameObject.SetActive(false);

                hit.collider.transform.GetChild(1).gameObject.SetActive(true);
                previosCube = hit.transform.gameObject;
            }
            if (hit.collider.tag == "Plane")
                if (null != previosCube)
                    previosCube.transform.GetChild(1).gameObject.SetActive(false);
        }
	}
}
