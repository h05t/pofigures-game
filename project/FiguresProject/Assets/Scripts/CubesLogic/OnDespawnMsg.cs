﻿using UnityEngine;
using System.Collections;

public class OnDespawnMsg : MonoBehaviour
{
    GameObject PhFoundationGo;
    FoundationTriger foundationTriger;
    //Type type;
    public string strType;

    void OnEnable()
    {
       //type = Type.GetType("FoundationTriger"); // как-то так можно передавать тип в кач-ве парам-ра???

        // Удаляем ссылки, чтобы после вытаскивания 
        // из пулла состояние объекта выглядело, как
        // будто его создали с помощ. new:
        PhFoundationGo = null;
        foundationTriger = null;
    }

    void OnDisable()
    {
        if (null != PhFoundationGo)
        {
           foundationTriger = PhFoundationGo.GetComponent<FoundationTriger>();
            foundationTriger.DecrimentBlockCounter();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if ("PhFoundation" == other.tag)
        {
            PhFoundationGo = other.gameObject;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if ("PhFoundation" == other.tag)
        {
            PhFoundationGo = null;
        }
    }
}
