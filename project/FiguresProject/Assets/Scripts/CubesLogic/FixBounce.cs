﻿using UnityEngine;
using System.Collections;

public class FixBounce : MonoBehaviour {
	void FixedUpdate()
	{
		var currentVelocity = rigidbody.velocity;
		
		if (currentVelocity.y <= 0f) 
			return;
		
		currentVelocity.y = 0f;
		
		rigidbody.velocity = currentVelocity;
	}
}
