﻿using UnityEngine;
using System.Collections;

public class ScreenShotLierDetect : MonoBehaviour
{
    public int layer;
    ScreenShotLierDetect otherScrnShotLierDetect;

    void OnTriggerEnter(Collider other)
    {
        if ("Plane" == other.gameObject.tag)
            layer = 0;
        else
            if ("Block" == other.gameObject.tag)
            {
                otherScrnShotLierDetect = other.transform.GetChild(4).GetComponent<ScreenShotLierDetect>();
                layer = otherScrnShotLierDetect.layer + 1;
            }
    }
}
