﻿using UnityEngine;
using System.Collections;

public class CubeOnDespawnMsg : MonoBehaviour
{
    GameObject PhGo;
    TriggerReaction triggerReaction;

    void OnEnable()
    {
       //type = Type.GetType("FoundationTriger"); // как-то так можно передавать тип в кач-ве парам-ра???

        // Удаляем ссылки, чтобы после вытаскивания 
        // из пулла состояние объекта выглядело, как
        // будто его создали с помощ. new:
       PhGo = null;
       triggerReaction = null;
    }

    void OnDisable()
    {
        if (null != PhGo)
        {
           triggerReaction = PhGo.GetComponent<TriggerReaction>();
           triggerReaction.DecrimentBlockCounter();
        }
    }

    void OnTriggerEnter(Collider other)
    {
       if ("PhantomTrigger" == other.tag)
       {
          PhGo = other.gameObject;
       }
    }

    void OnTriggerExit(Collider other)
    {
       if ("PhantomTrigger" == other.tag)
       {
          PhGo = null;
       }
    }
}

