﻿using UnityEngine;
using System.Collections;

public class PhantomConditions : MonoBehaviour
{
   public bool permittedToConstruct;
   
   // Дочерний объект, который хранит
   // текстуру для запрета строительства:
   GameObject cubeForForbidCunstruct;

   void Start()
   {
      cubeForForbidCunstruct = transform.GetChild(7).gameObject;
   }

   // Сбрасываем на настройки по умолчанию:
   void OnEnable()
   {
      permittedToConstruct = true;
   }

   // В зависимости от того разрешено ли строительство 
   // или нет будет выведена соответстующая текстура на
   // фантоме (т.е. только при запрете выведет).
   public void SetConstructPermition(bool permition)
   {
      permittedToConstruct = permition;
      cubeForForbidCunstruct.SetActive(!permittedToConstruct);
   }

}
