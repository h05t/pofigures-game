﻿using UnityEngine;
using System.Collections;

public class TriggerReaction : MonoBehaviour 
{
   public int i = 0;
   PhantomConditions phantomConditions;

   void Start()
   {
      phantomConditions = transform.parent.gameObject.GetComponent<PhantomConditions>();
   }

   // Сбрасываем на настройки по умолчанию:
    void OnEnable()
	{
      phantomConditions.SetConstructPermition(true);
      i = 0;
	}

	void OnTriggerEnter(Collider other) 
	{
      if ("Block" == other.tag)
      {
         ++i;
         phantomConditions.SetConstructPermition(false);
      }
	}
///
   void OnTriggerExit(Collider other)
   {
      if ("Block" == other.tag)
      {
         DecrimentBlockCounter();
      }
   }

   public void DecrimentBlockCounter()
   {
      if (0 < i)
      {
         --i;
         if (0 == i)
            phantomConditions.SetConstructPermition(true);
      }
   }
}
