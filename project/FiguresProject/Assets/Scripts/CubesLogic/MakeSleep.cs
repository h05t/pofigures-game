﻿using UnityEngine;
using System.Collections;

public class ParamForDeleteFromMatrx
{
	public Vector3 pos;
	public Vector3 blockScale;
	public Vector3 blockRotation;
	public bool justSpawned;

	public ParamForDeleteFromMatrx(GameObject cube)
	{
		SetParam(cube);
		justSpawned = true;
	}

	public void SetParam(GameObject cube)
	{
		pos = cube.transform.position;
		blockScale = cube.transform.localScale;
		blockRotation = cube.transform.rotation.eulerAngles;

		justSpawned = false;
	}
}

public class MakeSleep : MonoBehaviour {
    private bool notCeil = false;
	ParamForDeleteFromMatrx cubeParam;

	public void PrepareToDespawn()
	{
		cubeParam.justSpawned = true;
	}
	
	void Start () 
    {
		cubeParam = new ParamForDeleteFromMatrx(this.gameObject);
	}

	void FixedUpdate() 
    {
        if(this.rigidbody.IsSleeping() && notCeil)
        {
            Vector3 pos = new Vector3(this.transform.position.x, Mathf.Ceil(this.transform.position.y), this.transform.position.z);

            this.transform.position = pos;
            //Debug.Log("Sleep!" + Mathf.Ceil(this.transform.position.y));
            this.rigidbody.Sleep();

			cubeParam.SetParam(this.gameObject);
            notCeil = false;
        }
        
		// Работает только когда объект падает:
        if(true != this.rigidbody.IsSleeping() )
		{
            notCeil = true;
		}
	}
}
