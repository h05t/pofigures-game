using UnityEngine;
using System.Collections;

//
// ������ ������� ���������� �� ������
//
public class ScreenShotUpload : MonoBehaviour 
{
	int i = 0;
	//private bool showText = false;
	public GameObject SSCamera;
    private GetScreenShot getScreenShot;
   	
	// 
    void Start() 
	{
		SSCamera = GameObject.FindGameObjectWithTag ("ScreenshotCamera");//.GetComponent(Camera);
        getScreenShot = SSCamera.camera.GetComponent<GetScreenShot>();
    }
	//
	// ������, �.�. ����� �������� ����� ����������� ������ � ������ (�� �������) ��� ���������� ������! 
	// 
	void OnGUI() 
	{
		//if (showText) 
		//{
		//	GUI.Label(new Rect(100, 100, 200, 50), "Saving screen!");
		//}
	}

	//  
	public void GetScreenshotAndUploadIt(string url)
	{
		StartCoroutine(UploadPNG(url));
	}

	// ������ ��������� � ������ ������ -- http://forum.unity3d.com/threads/save-the-screenshot-to-a-file-from-webplayer.131759/
	//

	// Upload texture to server by URL
	public IEnumerator UploadPNG(string url) 
	{
		float startTime = Time.time;

		// get camera
		//Camera screenshotCam = SSCamera.camera;
        //screenshotCam.enabled = true; //!!!!!!!!!!!!!!!!!!!!!!! OnPostRender()!!! http://answers.unity3d.com/questions/360716/is-there-any-way-to-execute-readpixels-for-capturi.html

		// Show label with test at scene for a few seconds
		// show lable 2 seconds
		//yield return new WaitForSeconds (0.3f);
		//showText = true;

		// Take screenshot from camera
		//
		// get screenshot texture by CAMERA by ReadPixels (another RenderTexture)
//
        //Texture2D targetTexture = new Texture2D((int)screenshotCam.pixelWidth, // Screen.width
        //                                        (int)screenshotCam.pixelHeight, 
        //                                        TextureFormat.RGB24, false);
        //yield return new WaitForEndOfFrame();
        //targetTexture.ReadPixels(new Rect(0, 0, screenshotCam.pixelWidth, screenshotCam.pixelHeight), 0, 0);
		//targetTexture.Apply();

        SSCamera.camera.enabled = true;
        getScreenShot.StartGrabScrnShot();
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        SSCamera.camera.enabled = false;

        Texture2D targetTexture = getScreenShot.GetScrnShot();
//
		// Send screenshot to server 
		//
		// convert texture to byte array0
		byte[] pngData = targetTexture.EncodeToPNG();
		// send to server by WWWForm
        if (pngData != null) 
		{
			i++; // TODO - ������� ������, ����������!
			// PHP: ����� �������� �������� �������� ������ ������ ����� ���-�������� � ���������
			// �������, Unity - ������, ������� ������������� ����� � ��������� � ������� � 
			// ���-������, �������������� ��� ������ PHP, Perl, ... ���������! 
            WWWForm form = new WWWForm();
			//
			// TODO -- ���������, ��� ����� ������ ��� ���������� (���������)
			// TODO -- ��������� ���� ��� ������ ����������
			form.AddBinaryData("fileUpload", pngData, "screenshots/screenshot_" + i + ".png", "image/png");
            WWW www = new WWW(url, form);
            StartCoroutine(WaitForRequest(www));
        }

		//yield return new WaitForSeconds (2.3f);
		//showText = false;
		//screenshotCam.enabled = false;

		float endTime = Time.time - startTime;
		Debug.Log ("Taking screenshot time: " + endTime.ToString("0.000") + " seconds");
    }

	// WWW request answer
    IEnumerator WaitForRequest(WWW www) 
	{
        yield return www;
        if (www.error != null) 
            Debug.Log("Screenshot error: " + www.error);
		else 
            Debug.Log(www.text);
    }


}
