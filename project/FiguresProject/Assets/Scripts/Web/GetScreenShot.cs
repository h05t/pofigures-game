﻿using UnityEngine;
using System.Collections;

public class GetScreenShot : MonoBehaviour
{
    private bool grab = false;
    //private bool hereIsScrnShot = false; // нужно ли это???
    private Texture2D grabTexture;

    private void Start()
    {
        grabTexture = new Texture2D((int)this.camera.pixelWidth,
                                         (int)this.camera.pixelHeight,
                                         TextureFormat.RGB24, false);
    }

    private void OnPostRender()
    {
        if (grab)
        {
            grabTexture.ReadPixels(new Rect(0, 0, this.camera.pixelWidth, this.camera.pixelHeight), 0, 0);
            grabTexture.Apply();

            //hereIsScrnShot = true;
            grab = false;
        }
    }

    public void StartGrabScrnShot()
    {
        grab = true;
    }

    public Texture2D GetScrnShot()
    {
        grab = false;
        //hereIsScrnShot = false;

        return grabTexture;
    }
}
