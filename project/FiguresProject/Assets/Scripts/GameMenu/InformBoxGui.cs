﻿using UnityEngine;
using System.Collections;
using System;

public class InformBoxGui : MonoBehaviour
{
    string head;
    string bigCubeinf = "Большой\n";
    string smallCubeinf = "Маленький\n";

    float LabelTop = 47;
    float LabelWidth = 30;
    float LabelHeight = 20;

    int smCubePrice = 8; // (рублей)
    int bigCubePrice = 13; // (рублей)
    
    // Use this for initialization
    void Start()
    {
        head = String.Format("{0,43}\n\r\n\r",
                            "Информация о конструкции:");

        head += String.Format("{0,-13}{1,6}{2,6}{3,8}{4,7}{5,8}\n", //{0,23}{1,6}{2,8}{3,7}{4,8}\n
                                    "Кубик", "Белых", "Синих", "Зеленых", "Желтых", "Красных");
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnGUI()
    {
///
        GUI.skin.box.alignment = TextAnchor.UpperLeft;
        GUI.Box(new Rect(Screen.width - 333, 0, 333, 120), head + bigCubeinf + smallCubeinf);

        float LabelLeft = Screen.width - 240;
        GUI.Label(new Rect(LabelLeft, LabelTop, LabelWidth, LabelHeight), CubeCounterSingleton.GetInstance().bigWhiteCubeNum.ToString());
        GUI.Label(new Rect(LabelLeft, LabelTop + 15, LabelWidth, LabelHeight), CubeCounterSingleton.GetInstance().smWhiteCubeNum.ToString());

        GUI.Label(new Rect(LabelLeft = LabelLeft + 43, LabelTop, LabelWidth, LabelHeight), CubeCounterSingleton.GetInstance().bigBlueCubeNum.ToString());
        GUI.Label(new Rect(LabelLeft, LabelTop + 15, LabelWidth, LabelHeight), CubeCounterSingleton.GetInstance().smBlueCubeNum.ToString());

        GUI.Label(new Rect(LabelLeft = LabelLeft + 47, LabelTop, LabelWidth, LabelHeight), CubeCounterSingleton.GetInstance().bigGreenCubeNum.ToString());
        GUI.Label(new Rect(LabelLeft, LabelTop + 15, LabelWidth, LabelHeight), CubeCounterSingleton.GetInstance().smGreenCubeNum.ToString());

        GUI.Label(new Rect(LabelLeft = LabelLeft + 55, LabelTop, LabelWidth, LabelHeight), CubeCounterSingleton.GetInstance().bigYellowCubeNum.ToString());
        GUI.Label(new Rect(LabelLeft, LabelTop + 15, LabelWidth, LabelHeight), CubeCounterSingleton.GetInstance().smYellowCubeNum.ToString());

        GUI.Label(new Rect(LabelLeft = LabelLeft + 54, LabelTop, LabelWidth, LabelHeight), CubeCounterSingleton.GetInstance().bigRedCubeNum.ToString());
        GUI.Label(new Rect(LabelLeft, LabelTop + 15, LabelWidth, LabelHeight), CubeCounterSingleton.GetInstance().smRedCubeNum.ToString());

        //Всего больших кубиков:
        LabelLeft = Screen.width - 330;
        int LabelTopSecondSec = 47 + 15;

        // Общий вес набора:
        //GUI.Label(new Rect(LabelLeft + 196, LabelTopSecondSec + 33, 300, LabelHeight), "Цена(руб):");
        GUI.Label(new Rect(LabelLeft, LabelTopSecondSec + 15, 300, LabelHeight), "Общий вес набора:  " + CubeCounterSingleton.GetInstance().GetTotalWeight().ToString() + " (кг)");

        LabelTopSecondSec = LabelTopSecondSec + 15;
        int bigCubeTotalPrice = bigCubePrice * CubeCounterSingleton.GetInstance().BigCubeNum;
        int smCubeTotalPrice = smCubePrice * CubeCounterSingleton.GetInstance().smRedCubeNum;
        int summ = bigCubeTotalPrice + smCubeTotalPrice;
        GUI.Label(new Rect(LabelLeft, LabelTopSecondSec + 15, 300, LabelHeight), "Цена конструкции :  " + summ.ToString() + " (руб)");

        //GUI.Label(new Rect(LabelLeft, LabelTopSecondSec + 33, 300, LabelHeight), "Всего больших кубиков:    " + CubeCounterSingleton.GetInstance().BigCubeNum.ToString()); // аномалия в пробелах
        //GUI.Label(new Rect(LabelLeft, LabelTopSecondSec + 48, 300, LabelHeight), "Всего маленьких кубиков: " + CubeCounterSingleton.GetInstance().smRedCubeNum.ToString());

        ////GUIStyle customGuiStyle;
        //int bigCubeTotalPrice = bigCubePrice * CubeCounterSingleton.GetInstance().BigCubeNum;
        //int smCubeTotalPrice = smCubePrice * CubeCounterSingleton.GetInstance().smRedCubeNum;
        //int summ = bigCubeTotalPrice + smCubeTotalPrice;
        //GUI.Label(new Rect(LabelLeft + 196, LabelTopSecondSec + 33, 300, LabelHeight), /*"Цена:  " +*/ /*bigCubeTotalPrice.ToString()*/ "10000" + " (руб)");
        //GUI.Label(new Rect(LabelLeft + 196, LabelTopSecondSec + 48, 300, LabelHeight), /*"Цена:  " +*/ smCubeTotalPrice.ToString() + " (руб)");
        //GUI.Label(new Rect(LabelLeft + 197, LabelTopSecondSec + 63, 300, LabelHeight), "Итого: " + summ.ToString() + " (руб)"/*, customGuiStyle*/); // аномалия LabelLeft + 197
    }
}