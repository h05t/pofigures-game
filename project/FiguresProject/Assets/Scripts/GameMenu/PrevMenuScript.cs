﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Title screen script
/// </summary>
public class PrevMenuScript : MonoBehaviour
{
	private GUISkin skin;

	void Start()
	{
		skin = Resources.Load ("GUISkin") as GUISkin;
	}

	void OnGUI()
	{
		const int drawBuildingsWidthBtn = 140;
		const int drawBuildingsHeightBtn = 60;
		//
		const int bueMenuWidthBtn = 140;
		const int bueMenuHeightBtn = 60;
		//
		const int settingsWidthBtn = 100;
		const int settingsHeightBtn = 60;
		//
		const int exitWidthBtn = 60;
		const int exitHeightBtn = 60;

		GUI.skin = skin;
		// Determine the button's place on screen
		// Center in X, 2/3 of the height in Y
		Rect draw_buildings_scene = new Rect(
			Screen.width / 2 - (drawBuildingsWidthBtn / 2),
			(1 * Screen.height / 8) - (drawBuildingsHeightBtn / 2),
			drawBuildingsWidthBtn,
			drawBuildingsHeightBtn);
		// Draw a button to start the game
		if(GUI.Button(draw_buildings_scene,"Draw figure"))
		{
			Application.LoadLevel("main"); //  "DrawBuildings"
		}
		// Draw a button to Bye menu
		Rect bue_menu_scene = new Rect(
			Screen.width / 2 - (bueMenuWidthBtn / 2),
			(2 * Screen.height / 8) - (bueMenuHeightBtn / 2),
			bueMenuWidthBtn,
			bueMenuHeightBtn);
		// menu to push draw file to server
		// create file with draw information and 
		// do http put method to site server
		if(GUI.Button(bue_menu_scene,"Bue menu!"))
		{
			Application.LoadLevel("ByeMenuScene");
		}
		//
		Rect settings_scene = new Rect(
			Screen.width / 2 - (settingsWidthBtn / 2),
			(3 * Screen.height / 8) - (settingsHeightBtn / 2),
			settingsWidthBtn,
			settingsHeightBtn);
		// Draw a button to settings
		if(GUI.Button(settings_scene,"Settings"))
		{
			Application.LoadLevel("Settings");
		}
		Rect exit_btn = new Rect(
			Screen.width / 2 - (exitWidthBtn / 2),
			(4 * Screen.height / 8) - (exitHeightBtn / 2),
			exitWidthBtn,
			exitHeightBtn);
		// Draw a button to start the game
		if(GUI.Button(exit_btn,"Exit"))
		{
			//Application.LoadLevel("Exit");

			// Exit app
		}
	}
}
