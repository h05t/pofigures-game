﻿using UnityEngine;
using System.Collections;

public class CircleButton : MonoBehaviour 
{
	// Текстура для кнопки:
	private Texture2D texture = null;
	//private CircleButton[] subButtons;
	private ArrayList subButtonsList = new ArrayList();
	private bool doesSubButtonsList = false;

	// Прямоугольник в котором будет выведена текстура:
	private Rect rectangle;

	// mousePos - позиция курсора.
	// pressed - зажата ли левая кнопка мыши.
	// Возвратит true если нажатие Л.К.М. было на этой кнопке.
	public bool isPushed(Vector2 mousePos, bool pressed) 
	{
		// т.к. координаты мыши начинаются с левого нижнего угла,
		// а координаты на экране с левого верхнего, то преобразуем
		// координаты мыши по Y в координаты на экране:
		mousePos.Set(mousePos.x, Screen.height - mousePos.y);
		//mousePos = new Vector2(mousePos.x, Screen.height - mousePos.y);
		//
		//Debug.LogError(mousePos.x);
		//Debug.LogError(mousePos.y);
		//

		// Входит ли точка в область кнопки:
		if(rectangle.Contains(mousePos))
		{
			// получаем прозрачность(альфа канал) в точке, над которой находится курсор:
			float a = texture.GetPixel((int)mousePos.x, (int)(rectangle.y + rectangle.height - mousePos.y)).a;

			// если пиксель не прозрачный:
			if(1 == a)
			{
				if(pressed)
					return true;
			}
		}

		// Входит ли точка в область дочерних кнопок:

		return false;
	}

	public CircleButton(Vector2 pos, Vector2 size, Texture2D texture)
	{
		this.texture = texture;
		rectangle = new Rect (pos.x, pos.y, size.x, size.y);
	}

	// Отрисовка кнопки:
	public void Draw () 
	{
		GUI.DrawTexture (rectangle, texture);

		// Рисуем все выпадающие кнопки:
		if(true == doesSubButtonsList)
			for (int i = 0; i < subButtonsList.Count; i++) 
			{
				CircleButton tmpSubButton = (CircleButton)subButtonsList[i];
				tmpSubButton.Draw();
			}
	}

	public void AddNewButton (CircleButton nButton) 
	{
		subButtonsList.Add(nButton);
	}

	public CircleButton GetButton (int i) 
	{
		CircleButton tmpSubButton = (CircleButton)subButtonsList[i];
		return tmpSubButton;
	}

	public void DoesSubButtonListOpen (bool flag) 
	{
		doesSubButtonsList = flag;
	}
}
