﻿using UnityEngine;
using System.Collections;

using System.IO;
//using System.IO.BinaryWriter;


public class MenuController : MonoBehaviour 
{
	public Texture2D backgroundTexture;  	// фон для режима меню
	public const int mainMenuWidth = 200; 	// ширина кнопок в главном меню
	
	private int menutype = 0; 		// текущий тип меню - пригодится дальше
	private bool menuMode = true;	// включено ли меню
	private bool gameMode = false;	// запущена ли собственно игра -

	private GUISkin skin;


	public const int slotsAmount = 10;  
	public const int hN = 5;	
	public const int margin = 20;
	
	static private int vN = (int)Mathf.Ceil((float)slotsAmount/hN);	
	private Texture2D[] saveTexture = new Texture2D[slotsAmount];	
	private int slotSize = ((Screen.width*vN)/(Screen.height*hN) >= 1) ? Screen.height/(vN + 2) : Screen.width/(hN + 2);
	
	void Start()
	{
		skin = Resources.Load ("MainMenuGUISkin") as GUISkin;

		int gS = PlayerPrefs.GetInt("gamesSaved");
		for (int i = 0; i < slotsAmount && gS > 0; i++, gS/=2) 
			if (gS%2 != 0)
			{
				//saveTexture[i] = new Texture2D(Screen.width/4, Screen.height/4);  // тут тот же размер, в котором пишем на диск
				//saveTexture[i].LoadImage(System.IO.File.ReadAllBytes(Application.dataPath + "/tb/Slot" + i + ".png"));
			} 	// и адрес, конечно, тоже совпадает с тем, куда сохраняем.
	}

	// false  только до первого Load / New Game
	private void Awake(){
		DontDestroyOnLoad(this);	 // объект меню не будет разрушаться при загрузке новых сцен
	}
	
	void Update () {		
		if (Input.GetKeyDown(KeyCode.Escape)){  // warning! Это может быть не совсем очевидно: Input.GetKey()
			
			if(gameMode)	
			if (menutype == 0 || !menuMode){ // если мы в игре или на нижнем уровне меню
				switchGameActivity();		// остановливаем/запускаем игровые события
				//  (в данном случае просто движение камеры)
				menuMode = !menuMode;
			}
			menutype = 0;  // с более глубоких уровней меню возвращает в корень
		}					// из игрового режима грузит главное меню
	}
	
	private void OnGUI()
	{		
		GUI.skin = skin;

		if (menuMode)
		{
			if(backgroundTexture != null)		// очевидно, опционально
				GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backgroundTexture);
			
			switch (menutype)
			{
				case 0: drawMainMenu(); break;
				case 1: break;
				case 2: drawSaveLoadMenu(); break;
			}
		}
	}
	
	private void drawMainMenu()
	{	
		GUI.BeginGroup (new Rect (Screen.width / 2 - mainMenuWidth/2, Screen.height / 2 - 180, mainMenuWidth, 240));
		
		if (gameMode)
		if(GUI.Button(new Rect(0,0, mainMenuWidth,30) , "Вернуться в конструктор"))
		{
			menuMode = false;
			//switchMouseLook();
		}
		
		if(GUI.Button(new Rect(0, 40, mainMenuWidth, 30) , "Новоя постройка"))
		{
			menuMode = false;
			gameMode = true;
			Application.LoadLevel("MainScene");
		}
		
		if (gameMode)
			if(GUI.Button(new Rect(0, 2*40, mainMenuWidth, 30) , "Сохранить"))
				menutype = 1;
		
		if (GUI.Button (new Rect (0, ((gameMode) ? 3 : 2) * 40, mainMenuWidth, 30), "Загрузить")) 
		{ 
			menutype = 2;	// Почему не "2 + gameMode"? C#, nuff said.
		}
			
		if(GUI.Button(new Rect(0, ((gameMode)? 4 : 3)*40, mainMenuWidth, 30) , "Настройки")) {}
			
		if(GUI.Button(new Rect(0, ((gameMode)? 5 : 4)*40, mainMenuWidth, 30) , "Выйти из игры"))
		{
			Application.Quit();
		}

		GUI.EndGroup();	

	}
		
	void switchGameActivity() 
	{	// в данном случае cчитается, что на активной камере есть скрипт MouseLook,
		Camera mk = Camera.main;  // поворачивающий камеру по движению мыши. Вообще тут стоит
		//MouseLook ml = mk.GetComponent<MouseLook>();	 //  приостанавливать обработку всех событий ввода
		//if (ml != null) ml.enabled = !ml.enabled; 	// и всю динамику
	}

	private void drawSaveLoadMenu()
	{
		GUI.BeginGroup (new Rect (Screen.width / 2 -  (slotSize*hN - margin) / 2,
		                          Screen.height / 2 - (slotSize*vN - margin) / 2,
		                          slotSize*hN - margin, slotSize*vN + 40));
		
		for (int j = 0; j < vN; j++)
			for (int i = 0, curr = j*hN; (curr = j*hN + i) < slotsAmount; i++)
			{
				if (menutype == 2 && saveTexture[curr] == null)
					GUI.Box(new Rect(slotSize*i, slotSize*j, slotSize - margin, slotSize - margin), "");
				else 
					if(GUI.Button(new Rect(slotSize*i, slotSize*j, slotSize - margin, slotSize - margin),
				             saveTexture[curr]))
					{
						//if (menutype == 1) savestuff(curr);
						//else if (menutype == 2) loadstuff (curr);
					}				
			}
		
		if(GUI.Button(new Rect(slotSize*hN/2 - 100, slotSize*vN , 200, 30) , "Back"))
			menutype = 0;
		
		GUI.EndGroup();	
	}

	IEnumerator readScreen(int i)
	{
		yield return new WaitForEndOfFrame(); // так мы и избежим ошибок, и не заскриншотим само меню сохранения :)
		
		int adjustedWidth = Screen.height * 4/3; // допустим, мы хотим держать определенное соотношение сторон
		Texture2D tex1 = new Texture2D(adjustedWidth, Screen.height);
		// кропаем в свежесозданную текстуру нужный участок экрана
		tex1.ReadPixels(new Rect((Screen.width - adjustedWidth)/2, 0, adjustedWidth, Screen.height), 0, 0, true);
		tex1.Apply();
		
		//создаем новую текстуру нужного размера
		Texture2D tex = new Texture2D(Screen.height/3, Screen.height/4, TextureFormat.RGB24, true);
		tex.SetPixels(tex1.GetPixels(2)); // и применяем наш суровый и беспощадный ресайз	
		Destroy(tex1);
		tex.Apply();
		
		saveTexture[i] = tex;		// сохраняем в массив текстур
		
		FileStream fs = System.IO.File.Open(Application.dataPath + "/tb/Slot" + i + ".png", FileMode.Create); 
		BinaryWriter binary = new BinaryWriter(fs);
		binary.Write(tex.EncodeToPNG());	// и на диск
		fs.Close();
		
	}

	void savegame(int i)
	{
		
		PlayerPrefs.SetInt("slot" + i + "_Lvl", Application.loadedLevel);  // сохраняем текущий уровень
		
		// !
		Interface сi = Camera.main.GetComponent<Interface>();
		if (сi != null)  сi.save(i); 	// а основную работу с PlayerPrefs делегируем самому объекту
		
		menuMode = false; // выходим из меню
		switchGameActivity(); // возобновляем игру
		
		// немного простой битовой магии. Апдейтим информацию о функциональности слотов.
		// Как уже сказано, можно использовать и более читаемые варианты.
		// в текущей реализации это нам пригодится только при следующем запуске
		PlayerPrefs.SetInt("gamesSaved", PlayerPrefs.GetInt("gamesSaved") | (1 << i));
		
		StartCoroutine(readScreen(i)); // делаем скриншот уже после выхода из меню	
	}

	void loadgame(int i)
	{
		if (gameMode)	// меню Load доступно сразу, так что тут пригодится проверка
			switchGameActivity();
		
		PlayerPrefs.SetInt("Load", i); // сохраняем информацию о том, какой слот загружен
		
		Application.LoadLevel(PlayerPrefs.GetInt("slot" + i + "_Lvl"));  // загружаем нужный уровень
		
		menuMode = false;
		gameMode = true; // на случай, если игра ещё не запускалась
	}

		
}

public class Interface : MonoBehaviour {
	
	private Transform CurrentPlayerPosition;
	
	public virtual void Start () 
	{
		int load = PlayerPrefs.GetInt("Load");  // проверяем, не создан ли объект
		if (load >= 0)
		{  		// в рез-те загрузки сохранения
			//load(load);
			PlayerPrefs.SetInt("Load", -1);  // обнуляем Load
		}
	}
	
	public virtual void  save(int i)
	{	// получаем текущую позицию игрока и сохраняем все её параметры
		CurrentPlayerPosition = this.gameObject.transform.parent.transform;
		PlayerPrefs.SetFloat("slot" + i + "_PosX", CurrentPlayerPosition.position.x);
		PlayerPrefs.SetFloat("slot" + i + "_PosY", CurrentPlayerPosition.position.y);
		PlayerPrefs.SetFloat("slot" + i + "_PosZ", CurrentPlayerPosition.position.z);
	}
	
	public virtual void load(int i) {
		CurrentPlayerPosition = this.gameObject.transform.parent.transform; 
		
		// создаем новый вектор на основе загруженных параметров
		Vector3 PlayerPosition = new Vector3(PlayerPrefs.GetFloat("slot" + i + "_PosX"), 
		                                     PlayerPrefs.GetFloat("slot" + i + "_PosY"), PlayerPrefs.GetFloat("slot" + i + "_PosZ"));
		
		CurrentPlayerPosition.position = PlayerPosition;  // и применяем его, изменяя 
	}		// положение игрока на сохраненное
	
}


